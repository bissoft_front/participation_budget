'head': {
    defaults: {
        title: 'Індекс',
        useSocialMetaTags: false
    },
    home: {
        title: 'Головна',
        useSocialMetaTags: false
    },
    about: {
        title: 'Про проект',
        useSocialMetaTags: false
    },
    consultation: {
        title: 'Консультація',
        useSocialMetaTags: false
    },
    consultations: {
        title: 'Консультації',
        useSocialMetaTags: false
    },
    deputies: {
        title: 'Депутати',
        useSocialMetaTags: false
    },
    instructions: {
        title: 'Інструкції',
        useSocialMetaTags: false
    },
    law: {
        title: 'Нормативно-правова база',
        useSocialMetaTags: false
    },
    persona: {
        title: 'Депутат',
        useSocialMetaTags: false
    },
    news: {
        title: 'Новини',
        useSocialMetaTags: false
    },
    post: {
        title: 'Новина',
        useSocialMetaTags: false
    },
    boardComposition: {
        title: 'Склад Ради',
        useSocialMetaTags: false
    },
    rollcall: {
        title: 'Поіменне голосування',
        useSocialMetaTags: false
    }
}
,

__iconsData: {
    
        'acrobat': {
            width: '30px',
            height: '30px'
        },
    
        'add': {
            width: '30px',
            height: '30px'
        },
    
        'ajust': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-big': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-round-bright': {
            width: '30px',
            height: '30px'
        },
    
        'arrow-round': {
            width: '30px',
            height: '29px'
        },
    
        'arrow-small': {
            width: '30px',
            height: '30px'
        },
    
        'attach': {
            width: '30px',
            height: '30px'
        },
    
        'attention-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'backup': {
            width: '30px',
            height: '30px'
        },
    
        'calculator': {
            width: '30px',
            height: '30px'
        },
    
        'calendar': {
            width: '30px',
            height: '30px'
        },
    
        'camera': {
            width: '68px',
            height: '50px'
        },
    
        'cancel': {
            width: '30px',
            height: '30px'
        },
    
        'chat': {
            width: '30px',
            height: '30px'
        },
    
        'close-big': {
            width: '30px',
            height: '30px'
        },
    
        'close-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'close': {
            width: '30px',
            height: '30px'
        },
    
        'close_1': {
            width: '30px',
            height: '30px'
        },
    
        'code': {
            width: '30px',
            height: '30px'
        },
    
        'creative-commons': {
            width: '30px',
            height: '30px'
        },
    
        'done-rounded': {
            width: '30px',
            height: '30px'
        },
    
        'done': {
            width: '30px',
            height: '30px'
        },
    
        'dot': {
            width: '30px',
            height: '30px'
        },
    
        'download': {
            width: '30px',
            height: '30px'
        },
    
        'edit': {
            width: '30px',
            height: '30px'
        },
    
        'expand': {
            width: '30px',
            height: '30px'
        },
    
        'external-link': {
            width: '30px',
            height: '30px'
        },
    
        'facebook': {
            width: '30px',
            height: '30px'
        },
    
        'help': {
            width: '30px',
            height: '30px'
        },
    
        'hide': {
            width: '30px',
            height: '30px'
        },
    
        'hide_1': {
            width: '30px',
            height: '30px'
        },
    
        'ic-nav-1': {
            width: '78px',
            height: '92px'
        },
    
        'ic-nav-2': {
            width: '65.5px',
            height: '87.31px'
        },
    
        'ic-nav-3': {
            width: '73.22px',
            height: '96.1px'
        },
    
        'ic-nav-4': {
            width: '66.65px',
            height: '97.48px'
        },
    
        'ic-nav-5': {
            width: '101.14px',
            height: '95px'
        },
    
        'keyboard-arrow': {
            width: '30px',
            height: '30px'
        },
    
        'language': {
            width: '30px',
            height: '30px'
        },
    
        'link': {
            width: '30px',
            height: '30px'
        },
    
        'location': {
            width: '30px',
            height: '30px'
        },
    
        'menu': {
            width: '30px',
            height: '30px'
        },
    
        'minimize': {
            width: '30px',
            height: '30px'
        },
    
        'money': {
            width: '30px',
            height: '30px'
        },
    
        'phone': {
            width: '30px',
            height: '30px'
        },
    
        'piece': {
            width: '30px',
            height: '30px'
        },
    
        'play-new': {
            width: '48px',
            height: '48px'
        },
    
        'print': {
            width: '30px',
            height: '30px'
        },
    
        'reload': {
            width: '30px',
            height: '30px'
        },
    
        'reminder': {
            width: '30px',
            height: '30px'
        },
    
        'rss': {
            width: '30px',
            height: '30px'
        },
    
        'search': {
            width: '30px',
            height: '30px'
        },
    
        'settings-big': {
            width: '30px',
            height: '30px'
        },
    
        'settings-small': {
            width: '30px',
            height: '30px'
        },
    
        'shedule': {
            width: '30px',
            height: '30px'
        },
    
        'signal': {
            width: '30px',
            height: '30px'
        },
    
        'sound-off': {
            width: '30px',
            height: '30px'
        },
    
        'sound-on': {
            width: '30px',
            height: '30px'
        },
    
        'sound': {
            width: '30px',
            height: '30px'
        },
    
        'time': {
            width: '30px',
            height: '30px'
        },
    
        'timer': {
            width: '30px',
            height: '30px'
        },
    
        'user-big': {
            width: '30px',
            height: '30px'
        },
    
        'user': {
            width: '30px',
            height: '30px'
        },
    
        'visible': {
            width: '30px',
            height: '30px'
        },
    
        'wait': {
            width: '30px',
            height: '30px'
        },
    
        'warning': {
            width: '30px',
            height: '30px'
        },
    
        'youtube': {
            width: '30px',
            height: '30px'
        },
    
},

__pages: [{
                name: 'about',
                href: 'about.html'
             },{
                name: 'alerts',
                href: 'alerts.html'
             },{
                name: 'all',
                href: 'all.html'
             },{
                name: 'category',
                href: 'category.html'
             },{
                name: 'form-appeal',
                href: 'form-appeal.html'
             },{
                name: 'idea',
                href: 'idea.html'
             },{
                name: 'index-full',
                href: 'index-full.html'
             },{
                name: 'index',
                href: 'index.html'
             },{
                name: 'index_test',
                href: 'index_test.html'
             },{
                name: 'login',
                href: 'login.html'
             },{
                name: 'new-idea',
                href: 'new-idea.html'
             },{
                name: 'new-project',
                href: 'new-project.html'
             },{
                name: 'project',
                href: 'project.html'
             },{
                name: 'registration',
                href: 'registration.html'
             },{
                name: 'search',
                href: 'search.html'
             },{
                name: 'search_null',
                href: 'search_null.html'
             },{
                name: 'ui',
                href: 'ui.html'
             }]